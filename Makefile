# Makefile for Flask course: "build_sass_app_udemy"
# ... also see: Section 6, Lecture 25 (coding challenge)


WAIT_FOR = 5
DC = docker-compose
DCF = docker-compose -f docker/docker-compose-dev.yml

SVC = --force website
OPTS = # --force-rm --pull

## ---- DEV targets -- Docker and docker-compose

run: run_local

build_dev: requirements.txt
	${DCF} build ${OPTS} ${SVC}

up:
	${DCF} up -d
	sleep ${WAIT_FOR}; ${DCF} logs

stop:
	${DCF} stop

down:
	${DCF} down --volumes --remove-orphans

ps: status
status:
	${DCF} ps

LOGSF = # --follow
logs:
	${DCF} logs ${LOGSF}

clean_rmi:
	docker rmi $$(docker images -f "dangling=true" -q)

SVC = website
clean_docker: stop
	${DCF} rm -v ${SVC}

# "Web Development with Flask" targets
# To send Email(s): 
#   env MAIL_USERNAME=you@example.com MAIL_PASSWORD='uREmailPass' MAIL_SERVER=example.com FLASKY_ADMIN=you@example.com  make run_local
run_local:
	. venv/bin/activate; \
	export FLASK_DEBUG=1; \
	export FLASK_APP='flasky.app'; \
	python -m flask run --reload --host 127.0.0.1 --port 5000

CMD=shell
run_cmd:
	. venv/bin/activate; \
	export FLASK_DEBUG=1; \
	export FLASK_APP='flasky.app'; \
	python -m flask ${CMD}

run_manage:
	. venv/bin/activate; \
	export FLASK_DEBUG=1; \
	export FLASK_APP='flasky.app'; \
	python -m pdb manage.py ${CMD}

run_migrate_ch05:
	. venv/bin/activate; \
	export FLASK_DEBUG=1; \
	export FLASK_APP='flasky.app'; \
	manage.py db init; \
	manage.py db revision --autogenerate -m '1st revision'; \
	manage.py db upgrade

# NOTE: may have to do:
# sqlite> .tables
# alembic_version  role  user           
# sqlite> DROP TABLE alembic_version;


run_xxx:
	make clean
	/bin/rm -rf migrations

## ---- testing & clean-up
clean: clean_pycache

clean_pycache:
	find . | grep -E "(__pycache__|\.pyc|\.pyo$$)" | xargs rm -rf

lint:
	flake8 --exclude=env .

test:
	py.test tests
