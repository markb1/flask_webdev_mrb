import unittest
from flask import current_app
from flasky.app import create_app
from flasky.orm import OrmDB

db = None


class BasicsTestCase(unittest.TestCase):
    def setUp(self):
        global db
        self.app = create_app('testing')
        db = OrmDB()
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_app_exists(self):
        self.assertFalse(current_app is None)

    def test_app_is_testing(self):
        self.assertTrue(current_app.config['TESTING'])
