#!/usr/bin/env python

import os

from flask_script import (Manager, Shell)
from flask_migrate import (Migrate, MigrateCommand)

from flasky.app import create_app
from flasky.orm import OrmDB

app = create_app(os.getenv('FLASK_CONFIG', 'default'))
db = OrmDB()

manager = Manager(app)
migrate = Migrate(app, db)


def make_shell_context():
    from flasky.models import (Role, User)

    return dict(app=app, db=db, User=User, Role=Role)


manager.add_command('shell', Shell(make_context=make_shell_context))
manager.add_command('db', MigrateCommand)


from flasky import models  # noqa: E402, F401


@manager.command
def test():
    """Run the unit tests."""
    import unittest
    tests = unittest.TestLoader().discover('tests')
    unittest.TextTestRunner(verbosity=2).run(tests)


if __name__ == '__main__':
    manager.run()
