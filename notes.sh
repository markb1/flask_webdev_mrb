# 01-31-2019 Thu @ 05:48:54 PM EST

flask_webdev_mrb%  export FLASK_APP='app.app'; export FLASK_DEBUG=1
flask_webdev_mrb%  python -m flask shell
>>> from flasky.app import db
>>> db.create_all()

>>> from app.models import Role, User
admin_role = Role(name='Admin')
mod_role = Role(name='Moderator')
user_role = Role(name='User')
user_john = User(username='john', role=admin_role)
user_susan = User(username='susan', role=user_role)
user_david = User(username='david', role=user_role)


>>> print(vars(admin_role))
{'_sa_instance_state': <sqlalchemy.orm.state.InstanceState object at 0x7fcc90af7f28>, 'name': 'Admin', 'users': [<User john>]}
>>> print(admin_role.id)
None
>>> # db.session.add(admin_role)
>>> db.session.add_all([admin_role, mod_role, user_role, user_john, user_susan, user_david])
>>> print(admin_role.id)
1
>>> print(admin_role.name)
Admin

>>> admin_role.name = 'Administrator'
>>> db.session.add(admin_role)
>>> db.session.commit()

>>> wb = Role(name='WorkerBee')
>>> print(wb.id)
None
>>> db.session.add(wb)
>>> db.session.commit()
>>> print(wb.id)
4

>>> Role.query.all()
[<Role Administrator>, <Role Moderator>, <Role User>, <Role WorkerBee>]
>>> str(User.query.filter_by(role=user_role))
'SELECT user.id AS user_id, user.username AS user_username, user.role_id AS user_role_id \nFROM user \nWHERE ? = user.role_id'
>>> 
>>> User.query.all()
[<User john>, <User susan>, <User david>]
>>> User.query.filter_by(role=user_role)
<flask_sqlalchemy.BaseQuery object at 0x7fcc9006e4e0>
>>> str(User.query.filter_by(role=user_role))
'SELECT user.id AS user_id, user.username AS user_username, user.role_id AS user_role_id \nFROM user \nWHERE ? = user.role_id'

# new-run!!
% python -m flask shell
>>> from flasky.app import db
>>> from app.models import Role, User
>>> user_role = Role.query.filter_by(name='User').first()
>>> user_role.users
[<User susan>, <User david>]
>>> # with lazy='dynamic'
>>> user_role.users.all()
[<User susan>, <User david>]

# flask_migrate - db migrations
flask_webdev_mrb% make run_cmd CMD="db init"
  Creating directory /home/mbiggers/git/Python/Flask/flask_webdev_mrb/migrations ... done
  Generating /home/mbiggers/git/Python/Flask/flask_webdev_mrb/migrations/alembic.ini ... done
  Generating /home/mbiggers/git/Python/Flask/flask_webdev_mrb/migrations/env.py ... done
  ... (edited for brevitys)

  Please edit configuration/connection/logging settings in
  '/home/mbiggers/git/Python/Flask/flask_webdev_mrb/migrations/alembic.ini' before proceeding
