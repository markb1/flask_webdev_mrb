# $HOME/.bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific aliases and functions
alias d=dirs
alias pu=pushd
alias po=popd
#alias cd=pushd

alias h='fc -l'
alias c=clear
alias j=jobs
alias t='less -X'

alias ls='/bin/ls -FC --color=auto'
alias l='/bin/ls -lF --color=auto'

# some more ls aliases
alias ll='ls -lF'
alias la='ls -AF'
alias ls='ls -CF'

alias rm='/bin/rm -i'
alias df='df -h'

alias ru='sudo sv -v'
alias sr='screen -axR'

alias emn='emacsclient -n'
export EDITOR=emacsclient

export IGNOREEOF=5

if [ -f /usr/bin/remake ]; then
    alias make=remake
fi

HASH=$(if [[ $USER == root ]]; then echo "#"; else echo "%"; fi)
export PROMPT_DIRTRIM=1
## export PS1="\u@\H:\W > "

export PROMPT_COMMAND='PS1X=$(p="${PWD#${HOME}}"; [ "${PWD}" != "${p}" ] && printf "~";IFS=/; for q in ${p:1}; do printf /${q:0:1}; done; printf "${q:1}")'
export PS1='\u@\h ${PS1X}${HASH} '

set completion-ignore-case On
set editing-mode emacs
set menu-complete On
#set -o complete

function dg
{
    egrep -i ${1} ${HOME}/git/CsSpark/cli53.dump
}

function rmtsh
{
    usage="rmtsh user@hostname"

    if [ $# -lt 1 ]; then echo $usage; return; fi

    exec ssh -Y -c blowfish ${1}
}

function sshfwd
{
    usage="sshfwd user hostname port-num [localhost] [ssh-port]"
    if [ $# -lt 3 ]; then echo $usage; return; fi

    user=$1
    host=$2
    login=$user@$host

    loc=$3
    rem=$loc

    localhost=${4:-127.0.0.1}
    ssh=${5:-2022}

    ssh -C -c blowfish -p $ssh -Nf -L  $localhost:$loc:$host:$rem $login
}

# sshfwd mbiggers backupsrv1.totmkt.com 5666 127.0.0.101
# visit: http://127.0.0.1:8680/

alias cdn=_cd

function _cd()
{
    if [ $# -eq 0 ]; then
        set -- $HOME
    fi

    'cd' $@

    case $TERM in
        xterm*|rxvt*) echo -n "ESC]2;$(hostname)^G" ;;
        *) ;;
    esac
}

export TERM=xterm
export EDITOR=jove

export PATH="$HOME/bin:/usr/local/bin:/usr/local/sbin:/usr/sbin::/sbin:/usr/bin:/bin"
