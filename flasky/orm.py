import os
from flask_sqlalchemy import SQLAlchemy


class OrmDB(object):
    """
    a (Singleton) global, service-settings & status object
    """
    def __new__(cls, *args, **kwgs):
        if not hasattr(cls, "__instance__"):
            basedir = os.path.abspath(os.path.dirname(__file__))

            app = args[0]
            db_uri = app.config['DATABASE_URI_T'].format(basedir=basedir,
                                                         db_file='data.sqlite')
            app.config['SQLALCHEMY_DATABASE_URI'] = db_uri
            cls.__instance__ = SQLAlchemy(app)
        return cls.__instance__

    def __getattr__(self, name):
        return getattr(self.__instance__, name)

    def __setattr__(self, name):
        return setattr(self.__instance__, name)
