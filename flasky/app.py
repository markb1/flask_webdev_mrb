import os
from pprint import pformat

from datetime import datetime

from flask import (
    Flask,
    render_template,
    request,
    session,
    redirect,
    url_for,
)

from flask_bootstrap import Bootstrap
from flask_moment import Moment
from flask_mail import Mail

from flasky.orm import OrmDB
from flasky.email import send_email

from config import config


def create_app(config_name):
    """
    Create a Flask application using the app factory pattern.

    :return: Flask app
    """
    app = Flask(__name__, instance_relative_config=True)

    app.config.from_object(config[config_name])
    # app.config.from_object('config.settings')
    config[config_name].init_app(app)

    Bootstrap(app)
    Moment(app)  # noqa: F841
    app.config['flask_mail'] = Mail(app)

    db = OrmDB(app)
    db.init_app(app)

    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    return app


app = create_app(os.getenv('FLASK_CONFIG', 'default'))

# database-singleton 'db'
db = OrmDB()

app.logger.debug(app.config['SQLALCHEMY_DATABASE_URI'])


@app.route('/', methods=['GET', 'POST'])
def index():
    """
    Render a Hello World response.
    :return: Flask redirect or render_template

    using 'Post/Redirect/Get pattern'
    """
    from flasky.main.forms import NameForm

    form = NameForm()

    if form.validate_on_submit():
        # query for 'name'
        user = User.query.filter_by(username=form.name.data).first()

        if user is None:
            user = User(username=form.name.data)
            db.session.add(user)
            db.session.commit()
            session['known'] = False

            if app.config['FLASKY_ADMIN']:
                send_email(app, app.config['FLASKY_ADMIN'],
                           'New User',
                           'mail/new_user', user=user)
        else:
            session['known'] = True

        session['name'] = form.name.data
        return redirect(url_for('index'))

    return render_template('index.html',
                           form=form,
                           name=session.get('name'),
                           known=session.get('known', False),
                           current_time=datetime.utcnow(),
                           app_config=pformat(app.config))


@app.route('/user/<name>')
def user(name):
    """ """
    user_agent = request.headers.get('User-Agent')
    if name == 'crash.me':
        a = 1/0  # noqa:F841

    cfgvars = pformat(app.config)  # just some junk to render...

    return render_template('user.html',
                           name=name, browser=user_agent, stuff=cfgvars)


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


@app.errorhandler(500)
def internal_server_error(e):
    return render_template('500.html'), 500


# CLI-shell "live objects"
@app.shell_context_processor
def make_shell_context():
    return dict(db=db, User=User, Role=Role)
