from threading import Thread

from flask_mail import Message
from flask import render_template


# Ref: (modified from)
#   https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-xi-email-support


def do_thread(f):
    def wrapper(*args, **kwargs):
        thr = Thread(target=f, args=args, kwargs=kwargs)
        thr.start()
    return wrapper


def send_email(app, to, subject, template, **kwargs):
    """ """
    mail = app.config['flask_mail']  # set-up in 'flasky/app.py'

    @do_thread
    def send(app, log):
        with app.app_context():
            try:
                mail.send(msg)
            except Exception as e:
                log.error("[send_email] error: {0}".format(e))

    # [Flasky] New User
    msg = Message(subject="{} {}".format(app.config['FLASKY_MAIL_SUBJECT_PREFIX'],
                                         subject),
                  sender=app.config['FLASKY_MAIL_SENDER'],
                  recipients=[to],)

    msg.body = render_template(template + '.txt', **kwargs)
    msg.html = render_template(template + '.html', **kwargs)

    send(app, app.logger)
