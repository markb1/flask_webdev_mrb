=========================================
Flask Web Development, as modified by MRB
==========================================

:Description: using "Flask Web Development" OR&A book for web-dev study
:Revision: 1.1
:Author: Mark Biggers <biggers@utsl.com>
:Ref: get, set environment secrets, https://github.com/theskumar/python-dotenv
:Ref: Peewee, SQL ORM for Python, http://docs.peewee-orm.com/en/latest/peewee/quickstart.html
:Last update: 01-30-2019 Wed @ 11:44:41 AM EST


Introduction
------------
Working through the Book!   Using *docker-compose* in this Project, under ``docker/``, to do all development, debugging and testing.


Preliminary work
----------------
Get the old Flask Udemy-course code working again...
